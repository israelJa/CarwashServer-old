var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");

var jwt = require('jsonwebtoken');
var VerifyToken = require( '../auth/VerifyToken');
var VehicleType = require( './Vehicle').VehicleType;

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());

var bcrypt = require( 'bcryptjs')
var config = require( '../config');

//router.use( VerifyToken);

router.post( '/add', VerifyToken, function( req, res, next){
	// only admin is allowed to do this
	if( req.user.account_type === "admin"){
	}

});

router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		var vehicleType = new VehicleType(); 
		vehicleType.find( 'all',  function( err, rows){

			if( err) return res.status(500).send( {message: "Server error"});

			if( rows === null) return res.status(500).send( {message: "Server error"});

			// return data results
			return res.status( 200).send({vehicleType: rows});
		}
	}
});

router.get( '/vehicle_type/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type != 'admin'){
		// only admin can ccess list of users
		user = new Service();
		// get all users
		user.remove( 'id = '+req.body.id, function(){

		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;

