var database = require('../db');

var Service = database.extend({tableName: "service",});
var Service_criteria = database.extend({tableName: "service_criteria",});
var Cost = database.extend({tableName: "cost",});
var VehicleType = database.extend({tableName: "vehicle_type",});

module.exports = { VehicleType, Service_criteria, Service, Cost};
