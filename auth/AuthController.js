var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');

var jwt = require('jsonwebtoken');
var VerifyToken = require( './VerifyToken');
var User = require( '../user/User');

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());

// json web token
var bcrypt = require( 'bcryptjs')
var config = require( '../config');



router.post( '/login', function( req, res){
	user = new User();
	//user.find( 'first', { where: "email = '" + req.body.email + "'", fields: [ "id", "account_type", "store_ID", "email", "password"] }, function( err, row){
	user.query( "select user.id as id, account_type, store_ID, user.email as user_email, password, logo_path , contact_number, store.email as store_email, country, state, street from user inner join store on user.store_ID = store.id where user.email = '"+req.body.email+"'",  function( err, results){

		if( err) {
			console.log( err);	
			return res.status( 500).send( {message: 'Error on server'});
		}

		if( results.length === 0){ return res.status( 404).send( 'User not found'); }

		const row = results[0];
						 console.log( row);

		var passwordIsValid = bcrypt.compareSync( req.body.password, row.password);

		if( !passwordIsValid){ return res.status( 403).send({auth:false, token: null});}

		if( row.account_type == "normal"){

			// get the employee info
			user.query( "select first_name, last_name from user inner join userAccount on user_ID = user.id inner join employee on userAccount.employee_number = employee.employee_number where user.id="+row.id,
				function( err, results){
			
					// add this to object
					if( err){ 
						 console.log( err);
						return res.status( 500).send( {message:'Error: User data cannot be fetched'});
					}

					if( results.length < 1){ return res.status( 404).send( {message:'User data not found'}); }

					var token = jwt.sign({id:row.id, account_type: row.account_type}, config.secret, {expiresIn: 86400 });
					var userObj = Object.assign({},row,results[0]);
					console.log( userObj);
					return res.status( 200).send( {auth:true, token: token, user: userObj});
				}
			);

		}else{
		

			var token = jwt.sign({id:row.id, account_type: row.account_type}, config.secret, {expiresIn: 30 * 86400 });

			delete row.password;	

			// if admin manadatory signin once a month
			res.status( 200).send({auth: true, token: token, user: row});
		}
	});
});

router.get( '/me', VerifyToken, function( req, res, next){
	user = new User();
	//res.status( 200).send( decoded);
    console.log( "id = " + req.userId)
	user.find( 
		'first', 
		{ 
			fields: [ "id", "email", "store_ID", "account_type"], 
			where: "id = " + req.user.id
		}, 
		function( err, row){
			if( err){
				console.log( err);
				return res.status( 500).send( 'Error finding user');
			}

			if( !row) return res.status( 404).send( 'Could not find user');

			res.status( 200).send( row);
		}
	);
});

module.exports = router;
