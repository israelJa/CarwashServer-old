var database = require( "../db");

const EmployeeLog = database.extend({  tableName: "employee_log_book"});
const UserLog = database.extend({  tableName: "user_log_book"});

module.exports = { EmployeeLog, UserLog};
