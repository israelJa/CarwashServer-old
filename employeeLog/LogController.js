var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");
var EmployeeLog = require( "../employee/Employee");

var VerifyToken = require( '../auth/VerifyToken');

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());


//router.use( VerifyToken);
router.get( '/employee/:employee_number', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		var vehicleType = new VehicleType({
			employee_number: req.params.employee_number,
			time_in: Date.now(),	
		}); 
		vehicleType.find( 'all',  function( err, rows){

			if( err) return res.status(500).send( {message: "Server error"});

			if( rows === null) return res.status(500).send( {message: "Server error"});

			

			// return data results
			return res.status( 200).send({vehicle_types: rows});
		});
	}
});

router.get( '/:vehicle_type/all', VerifyToken, function( req, res, next){
	//return res.status(200).send( req.params);
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		var vehicleType = new VehicleType(); 
		//vehicleType.find( 'all', { where: "vehicle_type='"+req.params.vehicle_type+"'"}, function( err, rows){
		/*
		 * fetch all services+service criteria with vehicle type "req.params.vehicle_type"
		 */
		 vehicleType.query("select label, is_service_custom, available, cost.id as cost_ID, criteria_ID, cost.start_date as cost_start_date, cost.end_date as cost_end_date, service_cost, service_ID, vehicle_type_ID, service_criteria.start_date as criteria_start_date, service_criteria.end_date as criteria_end_date, vehicle_type from service inner join service_criteria on service_ID=service.id inner join cost on criteria_ID=service_criteria.id inner join vehicle_type on vehicle_type_ID=vehicle_type.id where is_service_custom=0 and available=1 and vehicle_type_ID="+req.params.vehicle_type, function( err, rows){

			if( err) return res.status(500).send( {message: "Server error"+err});

			if( rows === null) return res.status(404).send( {message: "Nothing found"});

			// return data results
			return res.status( 200).send({services: rows});
		});
	}
});

router.post( '/add', VerifyToken, function( req, res, next){
	// only admin is allowed to do this
	if( req.user.account_type === "admin"){

		var criterias =  req.body.criterias;
		var service = new Service({
			label:  req.body.label,
			is_service_custom:  req.body.custom_service
		});
		service.save( function( err, result, connection){
			if( err){
				return res.status( 500).send( {message: 'Server error: '});
			}

			if( result === null){
				return res.status(500).send( {message: "Server error"})
			}
			// save the criterias
			Service_criteria = Service_criteria();
			return connection.query( "insert into service_criterias( service_ID, vehicle_type_ID,start_date, service_cost) values (" + result.insertId+",?,?,?)", [criterias], null);  
			
		}); 
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		// only admin can ccess list of users
		service = new Service();
		// get all users
		service.find( 'all', function( err, results){
			if( err){
				console.log( err);
				return res.status( 500).send({ message: "Error on server"});
			}

			if( !results) return res.status(500).send({message: "No services found"})

			res.status( 200).send({ services: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type != 'admin'){
		// only admin can ccess list of users
		user = new Service();
		// get all users
		user.remove( 'id = '+req.body.id, function(){

		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;


