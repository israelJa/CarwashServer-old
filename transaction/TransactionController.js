var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");

var jwt = require('jsonwebtoken');
var VerifyToken = require( '../auth/VerifyToken');
var Transaction = require( './Transaction');
var Customer = require( '../customer/Customer');


var bcrypt = require( 'bcryptjs')
var config = require( '../config');

// idk
router.use( express.json());

router.post( '/add', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		var transaction = new Transaction();

		const statuses = { confirmed: 1, cancelled: 0};

		// query to add new user
		//transaction.query( 
		const addTransaction = "";
		const addPayment = "";
		const addStatus = "";
		const status = statuses.confirmed

		var transaction = new Transaction({
			customer_id:  req.body.customer_id,
			transaction_date:  Date.now(),
			cashier_ID:  req.body.cashier_id,
			total_cost:  req.body.total_cost,
			cancelled: 0
		});

		const payments = req.body.payments;


		//return res.status( 500).send( {message: "Server error"});

		transaction.save( function( err, result){
		
			if( err){
				return res.status( 500).send( {message: "Server error"});
			}

			where = "";

			if( result.length != 0){
				// update is transaction exist
				//return res.status( 200).send( {message: result});
				where = "id = "+result.id;
			}

				//  get the recently entered transaction

				// add all user
				//transaction.remove( {where: "id="+result.insertId});
				
				
				return res.status( 200).send( {message: "Transaction added"});
			//}); 
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});



//router.use( bodyParser.json());
//router.use( bodyParser.urlencoded({ extended: false}))
router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		const query = 
			"select transaction.id as transaction_number, transaction.total_cost, transaction_date, user.id as user_account_id, user.email as user_email, " +
				   "user.account_type, disabled as current_user_status, store1.id as employee_branch_store_id,  "+
				   "store1.state as employee_branch_state, store1.street as employee_branch_street, "+
				   "store1.email as employee_branch_store_email, store1.contact_number as employee_branch_store_contact_number, " +
				   "customer.id as customer_id, customer.contact_number as customer_contact_number, " +
				   "company, customer.country as customer_country, customer.state as customer_state, "+
				   "customer.street as customer_street, contact_person_first_name, contact_person_last_name, "+
				   "cur_customer "+
				   "from "+
				   "transaction left join customer on customer_ID = customer.id "+
				   "join user on cashier_ID=user.id "+
				   "inner join store store1 on store1.id = user.store_ID;";
		transaction = new Transaction();
		// get all users
		transaction.query( query, function( err, results, fields){
			if( err){
				return res.status( 500).send({ message: "Error on server"});
			}

			//if( results.length === 0) return res.status( 404).send( {message: "No transaction"});

			res.status( 200).send({ transactions: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		transaction = new Transaction();
		// get all users
		transaction.setSql( { terminated: true});
		transaction.save( {where:'id = '+req.body.id}, function( err, result){
			if( err){
				return res.status( 500).send( {message: "Server error"+err});
			}

			res.status( 200).send( {message: "Transaction Removed"});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;


