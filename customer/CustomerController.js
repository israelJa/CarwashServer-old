var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");

var jwt = require('jsonwebtoken');
var VerifyToken = require( '../auth/VerifyToken');
var Customer = require( './Customer');

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());

var bcrypt = require( 'bcryptjs')
var config = require( '../config');

//router.use( VerifyToken);

router.post( '/add', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){

		var customer = new Customer({
			company:  req.body.company,
			country:  req.body.country,
			state:  req.body.state,
			street:  req.body.street,
			contact_number:  req.body.contact_number,
			contact_person_first_name:  req.body.contact_person_first_name,
			contact_person_last_name:  req.body.contact_person_last_name,
			cur_customer: req.body.active_customer === "true" ? 1 : 0

		});

		//return res.status( 500).send( {message: "Server error"});

		customer.find( 'first', { where: "company = '"+req.body.company+"'"}, function( err, result){
		
			if( err){
				return res.status( 500).send( {message: "Server error"});
			}

			where = "";

			if( result.length != 0){
				// update is customer exist
				//return res.status( 200).send( {message: result});
				where = "id = "+result.id;
			}

			customer.save( where, function( err, result){
				if( err){
					return res.status( 500).send( {auth: false, message: 'Failed to add customer: '+err});
				}

				if( !result){
					return res.status(500).send( {message: "unable to add customer"})
				}

				//  get the recently entered customer
				return res.status( 200).send( {message: "Customer added"});
			}); 
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		user = new Customer();
		// get all users
		user.find( 'all', function( err, results){
			if( err){
				return res.status( 500).send({ message: "Error on server"});
			}

			if( results.length === 0) return res.status( 404).send( {message: "No cusomers"});

			res.status( 200).send({ customers: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		customer = new Customer();
		// get all users
		customer.setSql( { terminated: true});
		customer.save( {where:'id = '+req.body.id}, function( err, result){
			if( err){
				return res.status( 500).send( {message: "Server error"+err});
			}

			res.status( 200).send( {message: "Customer Removed"});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;

