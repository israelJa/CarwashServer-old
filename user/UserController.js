var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");

var jwt = require('jsonwebtoken');
var VerifyToken = require( '../auth/VerifyToken');
var User = require( './User');

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());

var bcrypt = require( 'bcryptjs')
var config = require( '../config');

//router.use( VerifyToken);

router.post( '/add', VerifyToken, function( req, res, next){
	// only admin is allowed to do this
	if( req.user.account_type == "admin"){
		var hashedPassword = bcrypt.hashSync( "password1", 8);

		//	employee_number:  req.body.employee_number,
		var user = new User({
			email:  req.body.email,
			store_ID:  req.body.store_ID,
			account_type:  req.body.account_type,
			password:  hashedPassword
		});
		// check if exists
		user.find( 'all', { where: "disabled=0 and email='"+req.body.email+"'"}, function( err, result){

			if( err) {
				console.log( err);
				return res.status(500).send({message:"Server error"});
			}

			if( result.length > 0 ){
				return res.status( 400).send({message: "User exists" });
			}

			user.save( function( err, result){
				if( err != null){
					console.log( err);
				console.log( "error free");
						user.remove( "id="+result.insertId);
					return res.status( 500).send( {auth: false, message: 'Failed to add account: '+err});
				}

				if( result == null){
					console.log( "empty entry");
						user.remove( "id="+result.insertId);
					return res.status(500).send( {message: "unable to add user"})
				}

				AnonModel.query( "insert into userAccount( user_ID, employee_number) values( " + result.insertId + "," + req.body.employee_number+")", function( err, results, fields){

					if( err != null){
						// remove the inserted row
						user.remove( "id="+result.insertId);
						return res.status( 500).send({ message: "Server Error"+err});
					}
					
					if( results == null){
						user.remove( "id="+result.insertId);
						return res.status( 500).send( {message: "unable to add user"});
					}
					console.log( "error er");

					return res.status( 200).send( {message: "user added"});
				});
			}); 
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.post( '/update', VerifyToken, function( req, res, next){
	//	employee_number:  req.body.employee_number,
	var user = new User({
		id: req.body.id,
		email:  req.body.email,
		account_type:  req.body.account_type,
	});

	if( req.hasOwnProperty( "password")){
		var hashedPassword = bcrypt.hashSync( "password1", 8);
		user.password = hashedPassword;
	}

	// check if exists
	user.find( 'all', { where: "disabled=0 and email='"+req.body.email+"'"}, function( err, result){
		if( err) {
			console.log( err);
			return res.status(500).send({message:"Server error"});
		}

		console.log( "Info: "+req.user.account_type + " -- " + req.user.id);

		if( req.user.account_type  == "admin" || req.user.id == result.id){
			user.save( function( err, result){
				if( err != null){
					console.log( err);
					return res.status( 500).send( {auth: false, message: 'Failed to add account: '+err});
				}else if( result == null){
					console.log( err);
					return res.status(500).send( {message: "Can't find user"});
				} else{
					res.sendStatus( 200);
				}
			}); 
		}
	});
});

router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin'){
		// only admin can ccess list of users
		user = new User();
		// get all users
		AnonModel.query( "select user.id as user_id, user.email, user.account_type, store.contact_number, store.country, store.state, store.street," +
			"emp.employee_number, emp.first_name, emp.last_name, emp.employee_email, emp.employee_contact_number, emp.cur_employee, emp.employee_store_id as employee_store_id, emp.position, emp.employee_store_country, emp.employee_store_state, emp.employee_store_street, emp.employee_store_email, emp.employee_store_contact_number "+
			"from user inner join store on user.store_ID = store.id " +
			"left join userAccount on user.id = userAccount.user_ID " +
			"left join (" + 
			"select e.employee_number, e.first_name, e.last_name, e.email as employee_email, e.contact_number as employee_contact_number, e.cur_employee, employee_store.id as employee_store_id, e.position, employee_store.country as employee_store_country, employee_store.state as employee_store_state, employee_store.street as employee_store_street, employee_store.email as employee_store_email, employee_store.contact_number as employee_store_contact_number" +
			" from employee as e inner join store as employee_store on e.store_ID = employee_store.id " +
			") as emp on userAccount.employee_number = emp.employee_number where disabled=0", function( err, results, fields){

				if( err){
					console.log( err);
					return res.status( 500).send({ message: "Error on server"});
				}

				res.status( 200).send({ users: results});
		});
		//user.find( 'all', { fields: [ "id"] }, function(){

		//});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type == 'admin'){
		// only admin can ccess list of users
		user = new User( { disable: 0});
		// get all users 
		user.save( { where: 'id = '+req.body.id}, function( err, result){
			if( err) return res.status( 500).send( {message: "Server error"});

			if(  !result) return res.status(500).send({message: "unable to remove user"})

			return res.status(200).send( {message: "User removed"});

		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;

