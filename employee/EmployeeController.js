var express = require('express');
var router = express.Router();
var bodyParser = require( 'body-parser');
var AnonModel = require( "../Model");

var jwt = require('jsonwebtoken');
var VerifyToken = require( '../auth/VerifyToken');
var Employee = require( './Employee').Employee;
var LogBook = require( './Employee').EmployeeLog;

router.use( bodyParser.urlencoded({ extended: false}))
router.use( bodyParser.json());

var bcrypt = require( 'bcryptjs')
var config = require( '../config');

//router.use( VerifyToken);
router.post( '/log/logout', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		const employee_number = req.body.employee_number;
		const date = new Date();

		var employeeLog = new LogBook({
			employee_number: employee_number,
			//time_out: date.getDay()+"-"+(date.getMonth()+1)+"-"+date.getFullYear(),	
			time_out: Date.now(),
			logged_in: 0,
		}); 

		employeeLog.find( 'first', {where: "employee_number="+employee_number+" and logged_in=1"}, function( err, rows){

			if( err){
				return res.status(500).send( {message: "Server error"});
			}

			if( rows.length == 0 ){
				return res.status(400).send( {message: "Employee not logged in"});
			}

			employeeLog.save( "id="+rows.id, function( err, result){
				if( err) return res.status(500).send( {message: "Server error"});

				if( result.length == 0){
					return res.status(400).send( {message: "Unable to complete action"});
				}
				return res.status( 200).send({message: "Successfully clocked out"});
			});

			// return data results
		});
	}
});



router.post( '/log/login', VerifyToken, function( req, res, next){
	if( req.user.account_type === 'admin' || req.user.account_type === 'normal'){
		var employee_number = req.body.employee_number;
		var employeeLog = new LogBook({
			employee_number: employee_number,
			time_in: Date.now(),	
			logged_in:1
		}); 
		employeeLog.find( 'first', {where: "employee_number="+employee_number+" and logged_in=0"}, function( err, rows){

			if( err){
				return res.status(500).send( {message: "Server error"});
			}

			if( rows.length != 0 ){
				return res.status(400).send( {message: "Employee already logged in"});
			}
			
			employeeLog.save( function( err, result){
				if( err){
					return res.status(500).send( {message: "Server error"});
				}

				return res.status(200).send( {message: "Successfully checked employee in"});
			});

			// return data results
			//return res.status( 200).send({data: rows});
		});
	}
});

router.post( '/add', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" ){
		//var hashedPassword = bcrypt.hashSync( req.body.password, 8);
		
		console.log( req.body);
		var employee = new Employee({
			employee_number: req.body.employee_number,
			contact_number:  req.body.contact_number,
			email:  req.body.email,
			first_name:  req.body.first_name,
			last_name:  req.body.last_name,
			store_ID: req.body.store_ID,
			position: req.body.position,
			date_joined: Date.now()//req.body.date_joined
		});

		console.log( "here");

		console.log( "here");
		employee.save( function( err, result){
			if( err){
				console.log("Error "+ err);
				return res.status( 500).send( {auth: false, message: 'Failed to add employee: '+err});
			}

			if( result.length == 0){
				console.log( result);
				return res.status(500).send( {message: "unable to add employee"})
			}

			return res.status( 200).send( {message: "Employee added"});
		}); 
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.post( '/update', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" ){
		//var hashedPassword = bcrypt.hashSync( req.body.password, 8);
		
		console.log( req.body);
		var employee = new Employee({
			contact_number:  req.body.contact_number,
			email:  req.body.email,
			first_name:  req.body.first_name,
			last_name:  req.body.last_name,
			store_ID: req.body.store_ID,
			position: req.body.position,
			date_joined: Date.now()//req.body.date_joined
		});

		console.log( "here");

		console.log( "herree");
		employee.save( "employee_number = "+req.body.employee_number, function( err, result){
		console.log( "herree");
			if( err){
				console.log("Error "+ err);
				return res.status( 500).send( {auth: false, message: 'Failed to add employee: '+err});
			}

			if( result.length == 0){
				console.log( result);
				return res.status(500).send( {message: "unable to add employee"})
			}

			return res.status( 200).send( {message: "Employee added"});
		}); 
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/washer/:store_id', VerifyToken, function( req, res, next){
	//return res.status(200).send( req.params.store_id);
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		employee = new Employee();
		// get all users
		//employee.find( 'all', { where: "store_ID='"+req.params.store_id+"' and position='car washer'"}, function( err, results){
		employee.query("select * from employee left join employee_log_book on employee.employee_number = employee_log_book.employee_number where store_ID='"+req.params.store_id+"' and position='car washer' and logged_in=1", function( err, results, fields){
			if( err){
				return res.status( 500).send({ message: "Error on server"});
			}

			if( results.length === 0){
				return res.status( 404).send( {message: "Employees not found"});
			}


			res.status( 200).send({ employees: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


router.get( '/checkin/:employee_number', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		logBook = new LogBook(
		);
		// get all users
		logBook.find( 'first', { where: "id = "+req.body.employee_number, groupBy: "id", orderDesc: true }, function( err, results){
			if( err){
				return res.status( 500).send({ message: "Error on server"});
			}

			if( results.length === 0) return res.status( 500).send( {message: "Employees not found"});


			res.status( 200).send({ employees: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/checkout/:employee_number', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		logBook = new LogBook();
		// get all users
		logBook.find( 'first', { where: "employee_number = "+req.params.employee_number , group: "id", orderDec: true}, function( err, results){
			if( err){
				return res.status( 500).send({ message: "Error on server"});
			}

			if( results.length === 0) return res.status( 404).send( {message: "Employees not found"});


			res.status( 200).send({ employees: results});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/all', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		employee = new Employee();
		// get all users

			employee.query( "select employee.employee_number, first_name, last_name, employee.email, employee.contact_number, cur_employee, store.id as store_id, position, country, state, street, store.email as store_email, store.contact_number as store_contact_number from employee inner join store on store.id = store_ID ", function( err, employees, fields){
				if( err) return res.status( 500).send( { mesage: "Error on Server"});

				if( employees.length == 0) return res.send( 404).send( { mesage: "Error on Server"});

				employee.query( "select store.id as store_id, country, state, street, store.email as store_email, store.contact_number as store_contact_number from store", function( err, stores, fields){
					if( err) return res.status( 500).send( { mesage: "Error on Server"});

					if( employees.length == 0) return res.send( 404).send( { mesage: "Error on Server"});
					res.status( 200).send({ stores: stores, employees: employees});
				});

			});

	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});

router.get( '/remove', VerifyToken, function( req, res, next){
	if( req.user.account_type === "admin" || req.user.account_type === "normal"){
		// only admin can ccess list of users
		employee = new Employee();
		// get all users
		employee.setSql( { cur_employee: false});
		employee.save( {where:'id = '+req.body.id}, function( err, result){
			if( err){
				return res.status( 500).send( {message: "Server error"+err});
			}

			if( !result) return res.status( 500).send( {message: "Employee not found"});

			res.status( 200).send( {message: "Employee Removed"});
		});
	}else{
		res.status( 401).send({message: "Restricted request"});
	}
});


module.exports = router;

