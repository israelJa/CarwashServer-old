var database = require('../db');

var Employee = database.extend({tableName: "employee",});
const EmployeeLog = database.extend({  tableName: "employee_log_book"});

module.exports = { Employee, EmployeeLog};

