var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var customerRouter = require('./customer/CustomerController');
var serviceRouter = require('./services/ServiceController');
var transactionRouter = require('./transaction/TransactionController');
var employeeRouter = require('./employee/EmployeeController');
var usersRouter = require('./user/UserController');
var AuthController = require( './auth/AuthController');

var app = express();

app.use(logger('dev'));
app.use(express.json());

app.use('/api/customer', customerRouter);
app.use('/api/employee', employeeRouter);
app.use('/api/service', serviceRouter);
app.use('/api/transaction', transactionRouter);
app.use('/api/user', usersRouter);
app.use('/api/auth', AuthController);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  return res.send({message: "Failed: " + err.message});
	//res.render('error');
});

module.exports = app;
